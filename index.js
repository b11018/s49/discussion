console.log(`s49 - JS - Reactive DOM with Fetch`);

// Syntax:
	// fetch('url', options)
		// url - this is the url which the request is to be made
		// options - an array of properties, optional parameter

// Show Posts
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data));

// Show Posts Function
const showPosts = (posts) => {
	let postEntries = "";
	posts.forEach(post  => {
		console.log(post);
		postEntries += `
			<div id= "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Add Post Function

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// cancels the event if it's cancellable, meaning that default action that belongs to the event will not occur
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Successfully added');
		// querySelector = null, resets the state of our input into blanks after submitting a new post
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
});

// Edit Post Function
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};

// Update Post Function
document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Post successfully updated');

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
});

// ACTIVITY:
/*
	>> Write the necessary code to delete a post
	>> Make a function called deletePost
	>> Pass a parameter id
	>> Use fetch method and the options
*/
const deletePost = (id) => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE',
		body: null,
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		alert('Successfully deleted');
		document.querySelector(`#post-${id}`).remove();
	})
};